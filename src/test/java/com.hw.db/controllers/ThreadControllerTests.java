package com.hw.db.controllers;

// Imports

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import com.hw.db.models.Thread;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.mockito.Mockito;
import org.mockito.MockedStatic;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;

// Tests

public class ThreadControllerTests {

    // Properties

    private Integer threadId;

    private String slug;

    private String title;

    private String forum;

    private String message;

    private String authorName;

    private Integer totalVotes;

    private User user;

    private Vote vote;

    private Thread thread;

    private ArrayList<Post> posts;

    // Test methods

    @BeforeEach
    void setup() {
        threadId = 123456789;
        slug = "aboba";
        title = "Thread 101";
        message = "Message";
        authorName = "Roman Nabiullin";
        forum = "International forum";
        totalVotes = 1000;

        thread = new Thread(
                authorName,
                Timestamp.from(Instant.now()),
                forum,
                message,
                slug,
                title,
                totalVotes
        );

        vote = new Vote(authorName, 0);

        user = new User();
        user.setNickname(authorName);

        Post post1 = new Post();
        post1.setAuthor(authorName);

        Post post2 = new Post();
        post2.setAuthor(authorName);

        posts = new ArrayList<>();
        posts.add(post1);
        posts.add(post2);
    }

    @Test
    @DisplayName("Correctly get thread info.")
    void correctlyGetThreadInfo() {
        try (MockedStatic<ThreadDAO> mockedTreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                configureMockedUserDAO(mockedUserDAO);
                configureMockedThreadDAO(mockedTreadDAO);

                ThreadController threadController = new ThreadController();

                ResponseEntity threadInfoResponse = threadController.info(slug);

                assertStatusCodeOk(threadInfoResponse);
                assertResponseBody(threadInfoResponse, thread);
            }
        }
    }

    @Test
    @DisplayName("Correctly get posts array for a thread.")
    void correctlyGetPosts() {
        try (MockedStatic<ThreadDAO> mockedTreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                configureMockedUserDAO(mockedUserDAO);
                configureMockedThreadDAO(mockedTreadDAO);

                ThreadController threadController = new ThreadController();

                ResponseEntity postsResponse = threadController.Posts(
                        slug,
                        100,
                        0,
                        "",
                        false
                );

                assertStatusCodeOk(postsResponse);
                assertResponseBody(postsResponse, posts);
            }
        }
    }

    @Test
    @DisplayName("Correctly create vote.")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> mockedTreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                configureMockedUserDAO(mockedUserDAO);
                configureMockedThreadDAO(mockedTreadDAO);

                ThreadController threadController = new ThreadController();

                ResponseEntity threadInfoResponse = threadController.createVote(slug, vote);

                Integer newVotes = totalVotes + vote.getVoice();

                assertStatusCodeOk(threadInfoResponse);
                assertResponseBody(threadInfoResponse, thread);

                assertEquals(newVotes, thread.getVotes());
            }
        }
    }

    // Private methods

    private void configureMockedUserDAO(MockedStatic<UserDAO> mockedUserDAO) {
        mockedUserDAO
                .when(() -> UserDAO.Info(authorName))
                .thenReturn(user);
    }

    private void configureMockedThreadDAO(MockedStatic<ThreadDAO> mockedThreadDAO) {
        mockedThreadDAO
                .when(() -> ThreadDAO.getThreadById(threadId))
                .thenReturn(thread);

        mockedThreadDAO
                .when(() -> ThreadDAO.getThreadBySlug(anyString()))
                .thenThrow(new DataRetrievalFailureException("Cannot determine slug."));

        mockedThreadDAO
                .when(() -> ThreadDAO.getThreadBySlug(slug))
                .thenReturn(thread);

        mockedThreadDAO
                .when(() -> ThreadDAO.createPosts(any(), any(), any()))
                .thenAnswer((Answer<Void>) invocation -> null);

        mockedThreadDAO
                .when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any()))
                .thenReturn(posts);
    }

    private void assertStatusCodeOk(ResponseEntity response) {
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    private <T> void assertResponseBody(ResponseEntity response, T body) {
        assertEquals(response.getBody(), body);
    }

}
